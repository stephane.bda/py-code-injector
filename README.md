# Py Code Injector

disclaimer : This code should not be used on anyone, this is strictly for educational purposes only.

Iptables Rules :
================

require netfilterqueue :
------------------------
```bash
apt-get install build-essential python-dev libnetfilter-queue-dev
pip install netfilterqueue
```

* if want to test / attack remote computer when MITM :

Trap all packets that usually go to Forward chain & put this (-j) to nfqueue (if test remote computer) :
```bash
iptables -I FORWARD -j NFQUEUE --queue-num 0
```

* if want to test / attack local Vm when MITM :

Trap all packets that usually go to Forward chain & put this (-j) to nfqueue (if test remote computer) :
```bash
iptables -I FORWARD -j NFQUEUE --queue-num 0
echo 1 > /proc/sys/net/ipv4/ip_forward
```

* if want to test / attack your computer

Trap all packets in our local computer (if test my computer) :
```bash
iptables -I OUTPUT -j NFQUEUE --queue-num 0
iptables -I INPUT -j NFQUEUE --queue-num 0
```

## Modify queue
Python code

## Accept encoding send by browser (gzip, deflate, ..) must be modified or delete otherwise server compress the response 

scapy set_payload is deprecated with new scapy
In certain case the load field content length prevents execution

## Exploitation :

1- Redirect all packets with Iptables rules / nfqueue

2- Run ARP_Spoof.py

3- Run code_injector.py

## Code Injection on HTTPS page :
Warning : HTTP /1.1 : allow the server to send the response in chunks, so the content length is not set in the response (there is no content-length)
- reassembled chunk or send the request in http 1.0


With SSL strip modify packet & file interceptor modify too, so attention of both interaction

0- Disabling Firewall if enable

1- Run sslstrip

2- Run arp_spoofer

3- Iptables rules for SSLSTRIP : 
```bash
iptables --flush 
iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 10000
```

4- Iptables rules to place packet in netfilterqueue (for analyse them)  :

Redirect all Output packets to netfilterqueue
```bash
iptables -I OUTPUT -j NFQUEUE --queue-num 0
```

Redirect all Input packets to netfilterqueue
```bash
iptables -I INPUT -j NFQUEUE --queue-num 0
```


